# Léeme (¡importante!)

Aquí encontrarás un experimento de fotones correlacionados, con su posterior análisis estadístico, que incluye la creación de un test de hipótesis. Para una correcta visualización de los archivos .ipynb, deberás, una vez abierto el mismo, hacer click en la opción "Display rendered file".
Puedes encontrar este ícono a la izquierda del botón "Edit" y a la derecha del botón "Display source", todos localizados en el encabezado del archivo.


Ante cualquier duda, escribir a lcabral.imd@gmail.com